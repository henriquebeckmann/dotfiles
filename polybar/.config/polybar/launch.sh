killall -q polybar

if type "xrandr"; then
  for m in $(xrandr --query | grep " connected" | cut -d" " -f1); do
    MONITOR=$m polybar --reload main &
  done
  # for m in $(xrandr --query | awk '/primary/ {printf $1}'); do
  #   MONITOR=$m polybar --reload main &
  # done
else
  polybar --reload main &
fi
