#!/bin/bash

vol="$(pacmd list-sinks | awk '/front-left:/ { printf("%d"), $5; exit }')"
status="$(pacmd list-sinks | awk '/muted:/ { printf("%s"), $2; exit }')"
if [ $status == 'no' ]; then
  if [ "$vol" -gt 70 ]; then
    printf " $vol%%"
  elif [ "$vol" -gt 30 ]; then
    printf " $vol%%"
  else
    printf " $vol%%"
  fi
else
  printf " $vol%%"
fi

case $BUTTON in
	3) pactl set-sink-mute 0 toggle; pkill -RTMIN+3 dwmblocks;;
	4) pactl set-sink-volume 0 +5%; pkill -RTMIN+3 dwmblocks;;
	5) pactl set-sink-volume 0 -5%; pkill -RTMIN+3 dwmblocks;;
esac
