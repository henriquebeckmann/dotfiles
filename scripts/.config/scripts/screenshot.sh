#!/bin/bash

if [ ! -d "$HOME/Pictures/screenshots" ]; then
  mkdir -p "$HOME/Pictures/screenshots"
fi

date_format=$(date +%Y-%m-%d_%H-%M-%S)
filename="${date_format}.png"
focused_monitor=$(xrandr --listactivemonitors | awk 'NR==2 {printf ("%d"), $1}')

if [[ -z $1 ]]; then
  prefix="screenshot"
  filename="${prefix}_${filename}"
else
  if [[ $1 == "-u" ]]; then
    prefix="window"
    filename="${prefix}_${filename}"
  elif [[ $1 == "-s" ]]; then
    prefix="select"
    filename="${prefix}_${filename}"
  elif [[ $1 == "-M" ]]; then
    prefix="monitor_$focused_monitor"
    filename="${prefix}_${filename}"
  else
    echo "Parâmetro inválido!"
    exit 1
  fi
fi

filepath="$HOME/Pictures/screenshots/$filename"

if [[ $1 == "-u" ]]; then
  scrot -u "$filepath"
elif [[ $1 == "-s" ]]; then
  error=$(scrot -s "$filepath" 2>&1 >/dev/null)
  if [[ -n $error ]]; then
    exit 1
  fi
elif [[ $1 == "-M" ]]; then
  scrot -M $focused_monitor "$filepath"
else
  scrot "$filepath"
fi

if [[ -n $filename ]]; then
  xclip -selection clipboard -t image/png -i "$filepath"

  message="A screenshot <b>$filename</b> foi salva em <b>$filepath</b>..."
  notify-send "ScreenShot" "$message"
fi
