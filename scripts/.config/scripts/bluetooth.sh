#!/bin/sh
if [ $(bluetoothctl show | grep "Powered: yes" | wc -c) -ne 0 ]
then
  device_name=$(bluetoothctl info | grep "Name" | awk -F': ' '{print $2}')
  if [ ! -z "$device_name" ]
  then
    echo "%{F#3b82f6} $device_name"
  else
    echo " on"
  fi
else
  echo "%{F#707880} off"
fi

