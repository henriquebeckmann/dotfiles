#!/bin/bash

tag="Volume"

volume="$(pacmd list-sinks | awk '/front-left:/ { printf("%d"), $5; exit }')"
mute="$(pacmd list-sinks | awk '/muted:/ { printf("%s"), $2; exit }')"
if [[ $mute == 'yes' || $volume == 0 ]]; then
    # Show the sound muted notification
    dunstify -a "changeVolume" -h string:x-dunst-stack-tag:$tag "Volume muted"
else
    # Show the volume notification
    dunstify -a "changeVolume" -h string:x-dunst-stack-tag:$tag \
    -h int:value:"$volume" "Volume: ${volume}%"
fi
