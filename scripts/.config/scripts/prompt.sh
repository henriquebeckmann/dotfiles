#!/bin/bash

OPTIONS="bookmark\nfind\npassword\nkill process"

CHOICE=$(echo -e $OPTIONS | dmenu -p "prompt:")

case $CHOICE in
  bookmark)
    dbookmark
    ;;
  find)
    ~/.config/scripts/find.sh
    ;;
  password)
    passmenu -i -l 12 -p "password:"
    ;;
  "kill process")
    ~/.config/scripts/kill_process.sh
    ;;
  *)
    echo "Opção inválida."
    ;;
esac

