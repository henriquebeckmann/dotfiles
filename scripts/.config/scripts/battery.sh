#!/bin/bash

level="$(acpi | awk '{ printf("%d"), $4 }')"
status="$(acpi | sed "s/,//g" | awk '{ print $3 }')"

if [ "$status" = 'Charging' -o "$status" = 'Not' ]; then
    if [ $level = 0 ]; then
        # printf " 100%%"
        echo "100%"
    else
        # printf " $level%%"
        echo "$level%"
    fi

else
    # printf " $level%%"
    echo "$level%"
fi
