#!/bin/bash

selected_process_info=$(procs --sortd=mem | dmenu -i -l 12 -p "process kill:")

selected_pid=$(echo "$selected_process_info" | awk '{print $1}')

if [ -n "$selected_pid" ]; then
    confirm=$(echo -e "yes\nno" | dmenu -p "process kill $selected_pid?")

    if [ "$confirm" = "yes" ]; then
        kill -9 "$selected_pid"
        if [ $? -eq 0 ]; then
          notify-send "Process Killed" "Process with PID $selected_pid was killed successfully."
        else
          notify-send "Process Kill Failed" "Failed to kill the process with PID $selected_pid."
        fi
    fi
fi
