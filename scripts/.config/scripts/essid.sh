#!/bin/bash

essid="$(iwgetid -r)"

case "$(cat /sys/class/net/w*/operstate 2>/dev/null)" in
  up) printf " $essid";;
  down) printf " down";;
esac
