#!/bin/bash

mem_use="$(free -t | awk '/^Mem/ { printf("%d%"), $3/$2*100 }')"

printf " $mem_use%"
