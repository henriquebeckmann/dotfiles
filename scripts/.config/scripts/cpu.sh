#!/bin/bash

cpu_val="$(awk '{ printf("%d"), $1 }' /proc/loadavg)"

printf "  $cpu_val%%"
