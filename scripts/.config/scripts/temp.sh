#!/bin/bash

temp="$(sensors | awk '/^Core 0/ { printf("%d"), $3 }')"

printf " $temp°C"
