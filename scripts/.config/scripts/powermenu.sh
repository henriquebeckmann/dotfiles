#!/bin/bash

case "$(echo -e "lock\nlogout\nsuspend\nreboot\nshutdown" | dmenu -i -p "system:")" in
    lock) i3lock -c "#000000";;
    logout) i3-msg exit;;
    suspend)
        mpc -q pause
        pactl set-sink-mute @DEFAULT_SINK@ true && pkill -SIGRTMIN+10 i3blocks
        i3lock -c "#000000"
        systemctl suspend;;
    reboot) systemctl reboot;;
    shutdown) systemctl poweroff -i;;
esac
