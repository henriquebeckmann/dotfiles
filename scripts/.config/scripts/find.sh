#!/bin/bash

files=$(fd . $HOME | dmenu -i -l 12 -p "find:")
if [ -n "$files" ]; then
    chosen_program=$(echo -e "xdg-open\nbrowser\ngimp\nnvim\npcmanfm\nsxiv\nzathura\nextract" | dmenu -i -p "Choose program:")
    case "$chosen_program" in
      "xdg-open") xdg-open "$files" ;;
      "browser") $BROWSER "$files" ;;
      "gimp") gimp "$files" ;;
      "nvim") nvim "$files" ;;
      "pcmanfm") pcmanfm "$files" ;;
      "sxiv") sxiv "$files" ;;
      "zathura") zathura "$files" ;;
      "extract") extract "$files" ;;
        *) $chosen_program "$files" ;;
    esac
fi
