# Dotfiles
A collection of settings to place in the user's home directory is usually prefixed with a period, hence the name dotfiles. This repostory is really meant for my personal backup, but everyone is invited to use these features. You may not always be up to date with my current configuration.

## Installation

### Requirements
* git
* stow

Installing git on debian derivatives:
```shell
apt install git
```

Installing stow on debian derivatives:
```shell
apt install stow
```

## Usage
Clone this repository into your $HOME directory or ~

```shell
git clone https://gitlab.com/henriquebeckmann/dotfiles ~
```
Run stow to symlink everything or just select what you want

```shell
stow */ # Everything (the '/' ignores the README)
```

```shell
stow zsh # Just my zsh config
```

## Programs
An updated list of all the programs I use can be found in the programs directory