# Path to oh-my-zsh installation
export ZSH="$HOME/.oh-my-zsh"

# oh-my-zsh plugins
plugins=(
  zsh-autosuggestions
  zsh-syntax-highlighting
  dotenv
)

source $ZSH/oh-my-zsh.sh

# History
HISTFILE=~/.zsh_history
HISTSIZE=1000
SAVEHIST=1000

ZSH_DOTENV_FILE=.env

# Starship prompt
eval "$(starship init zsh)"

# Variables
# export TERM="kitty"
export BROWSER="brave"
export EDITOR="nvim"

# Alias
alias vi="nvim"
alias py="python"
alias s="sudo su"
alias pacman="sudo pacman"
alias p="sudo pacman"
alias cp="cp -iv"
alias rm="rm -iv"
alias mv="mv -iv"
alias mkdir="mkdir -pv"
alias docker="podman"
alias compose="podman-compose"
alias ls="exa --color=automatic --group-directories-first"
alias la="exa -la --color=automatic --group-directories-first"
alias ll="exa -l --color=automatic --group-directories-first"
alias lg="lazygit"
alias y="yazi"
alias free="free -m"
alias df="df -h"
alias cat="bat"
alias q="exit"

# Keybindings
bindkey "^ " autosuggest-accept

# Exports
export PATH="/home/henrique/.local/bin:$PATH"
export ANDROID_HOME=/home/henrique/Android/Sdk

# Colors.
RED='\e[1;31m'
GREEN='\e[1;32m'
YELLOW='\e[1;33m'
BLUE='\e[1;34m'
PURPLE='\e[1;35m'
CYAN='\e[1;36m'
NC='\e[0m'

# Functions
function confirm() {
  local answer
  echo -ne "sure you want to run '${YELLOW}$*${NC}' [yN]? "
  read -q answer
  echo
  if [[ "${answer}" =~ ^[Yy]$ ]]; then
    command "${@}"
  else
    return 1
  fi
}

alert() {
    local command="$1"
    local message="Execution completed: $command"
    eval "$command" &
    local process_id=$!
    wait $process_id
    notify-send "Alert" "$message"
}

function extract() {
  if [ -z "$1" ]; then
    # display usage if no parameters given
    echo "Usage: extract <path/file_name>.<zip|rar|bz2|gz|tar|tbz2|tgz|Z|7z|xz|ex|tar.bz2|tar.gz|tar.xz>"
    echo "       extract <path/file_name_1.ext> [path/file_name_2.ext] [path/file_name_3.ext]"
  else
    for n in "$@"
    do
      if [ -f "$n" ] ; then
        case "${n%,}" in
          *.cbt|*.tar.bz2|*.tar.gz|*.tar.xz|*.tbz2|*.tgz|*.txz|*.tar)
                       tar xvf "$n"       ;;
          *.lzma)      unlzma ./"$n"      ;;
          *.bz2)       bunzip2 ./"$n"     ;;
          *.cbr|*.rar) unrar x -ad ./"$n" ;;
          *.gz)        gunzip ./"$n"      ;;
          *.cbz|*.epub|*.zip) unzip ./"$n";;
          *.z)         uncompress ./"$n"  ;;
          *.7z|*.arj|*.cab|*.cb7|*.chm|*.deb|*.dmg|*.iso|*.lzh|*.msi|*.pkg|*.rpm|*.udf|*.wim|*.xar)
                       7z x ./"$n"        ;;
          *.xz)        unxz ./"$n"        ;;
          *.exe)       cabextract ./"$n"  ;;
          *.cpio)      cpio -id < ./"$n"  ;;
          *.cba|*.ace) unace x ./"$n"     ;;
          *)
            echo "extract: '$n' - unknown archive method"
            return 1
            ;;
        esac
      else
        echo "'$n' - file does not exist"
        return 1
      fi
    done
  fi
}

cdfzf() {
  SELECT=$(fd --base-directory "$HOME" --type d --hidden --exclude .git | fzf-tmux -p --reverse)

    if [[ -n $SELECT ]]; then
      FULL_PATH="$HOME/$SELECT"
      cd "$FULL_PATH"

      if [ -n "$TMUX" ]; then
        SESSION_NAME=$(basename "$PWD")
        tmux new-session -c "$FULL_PATH"
        tmux rename-session "$SESSION_NAME"
      fi
      zle reset-prompt
    fi
  }

  zle -N cdfzf_widget cdfzf
  bindkey '^[c' cdfzf_widget

fzf_zsh_history() {
  local selected_command
  selected_command=$(
  cat ~/.zsh_history |
    sed -r 's/:.+;//' |
    sort -u |
    fzf-tmux -p --reverse --preview 'echo {}' --preview-window=up:1:wrap |
    awk '{print $0}' |
    sed -r 's/;//'
  )

  if [ -n "$selected_command" ]; then
    BUFFER="$selected_command"
    # zle accept-line
  fi
}

# zoxide
eval "$(zoxide init --cmd cd zsh)"

# Associe a função a um atalho de teclado, por exemplo, Ctrl + R
zle -N fzf_zsh_history
bindkey '^R' fzf_zsh_history

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="$HOME/.sdkman"
[[ -s "$HOME/.sdkman/bin/sdkman-init.sh" ]] && source "$HOME/.sdkman/bin/sdkman-init.sh"
